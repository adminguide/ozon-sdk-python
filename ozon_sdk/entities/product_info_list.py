from .base.ozon_base_entity import BaseEntity
from .item import Item

class ProductInfoList(BaseEntity):
    """Получить список товаров по идентификаторам"""

    items: list[Item]