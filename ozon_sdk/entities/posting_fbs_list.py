from .base.ozon_base_entity import BaseEntity
from .fbs_posting import FBSPosting

class PostingFBSList(BaseEntity):
    has_next: bool
    postings: list[FBSPosting]