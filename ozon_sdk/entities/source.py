from .base.ozon_base_entity import BaseEntity

class Source(BaseEntity):
    is_enabled: bool
    sku: int
    source: str