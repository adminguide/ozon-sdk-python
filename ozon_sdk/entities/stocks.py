from .base.ozon_base_entity import BaseEntity

class Stocks(BaseEntity):
    coming: int
    present: int
    reserved: int