from .base.ozon_base_entity import BaseEntity

class ProductListItems(BaseEntity):
    """Класс для списка товаров"""

    offer_id: str
    product_id: int