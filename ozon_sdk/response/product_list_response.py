from .base.ozon_base_response import BaseResponse
from ..entities.product_list import ProductList

class ProductListResponse(BaseResponse):
    result: ProductList