from .base.ozon_base_response import BaseResponse
from ..entities.product_info import ProductInfo

class ProductInfoResponse(BaseResponse):
    result: ProductInfo