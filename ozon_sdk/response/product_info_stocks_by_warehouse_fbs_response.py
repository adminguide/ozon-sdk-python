from .base.ozon_base_response import BaseResponse
from ..entities.product_info_stocks_by_warehouse_fbs import ProductInfoStocksByWarehouseFBS

class ProductInfoStocksByWarehouseFBSResponse(BaseResponse):
    result: list[ProductInfoStocksByWarehouseFBS]