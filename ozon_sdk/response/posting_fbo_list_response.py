from .base.ozon_base_response import BaseResponse
from ..entities.posting_fbo_list import PostingFBOList

class PostingFBOListResponse(BaseResponse):
    result: list[PostingFBOList] = []