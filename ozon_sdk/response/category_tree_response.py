from .base.ozon_base_response import BaseResponse
from ..entities.category_tree import CategoryTree

class CategoryTreeResponse(BaseResponse):
    result: list[CategoryTree]