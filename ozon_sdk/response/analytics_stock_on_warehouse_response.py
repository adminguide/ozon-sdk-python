from .base.ozon_base_response import BaseResponse
from ..entities.total_item import TotalItem
from ..entities.wh_item import WhItem
#from ..entities import *
from datetime import datetime

class AnalyticsStockOnWarehouseResponse(BaseResponse):
    """Отчёт по остаткам и товарам"""

    timestamp: datetime
    total_items: list[TotalItem]
    wh_items: list[WhItem]

    class Config:
        orm_mode = True