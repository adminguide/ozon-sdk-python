from .base.ozon_base_response import BaseResponse
from ..entities.product_info_stocks import ProductInfoStocks

class ProductInfoStocksResponse(BaseResponse):
    result: ProductInfoStocks