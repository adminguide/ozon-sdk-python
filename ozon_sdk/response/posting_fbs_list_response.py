from .base.ozon_base_response import BaseResponse
from ..entities.posting_fbs_list import PostingFBSList

class PostingFBSListResponse(BaseResponse):
    result: PostingFBSList