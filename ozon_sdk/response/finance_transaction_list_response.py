from .base.ozon_base_response import BaseResponse
from ..entities.finance_transaction_list import FinanceTransactionList

class FinanceTransactionListResponse(BaseResponse):
    result: FinanceTransactionList = None