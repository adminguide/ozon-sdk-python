from .base.ozon_base_response import BaseResponse
from ..entities.product_info_list import ProductInfoList

class ProductInfoListResponse(BaseResponse):
    result: ProductInfoList